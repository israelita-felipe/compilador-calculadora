/*
Author: Vinicius Ferreira de Souza
*/

%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "estruturas.h"  

%}

%union{
	int ival;
	float fval;
    	struct Node *node;
}

%token<ival> INT VAR TINT TFLOAT FLOAT
%token<fval> 
%type<node> expr varlist

%left '+' '-'
%left '*' '/'
%left '^'

%%
program:
	program stmt_list '\n'	{printSymbolTable();}
	| program expr '\n' 	{printSymbolTable();}
	| 
	; 





stmt:
	expr ';'		{printSymbolTable();}
	| VAR '=' expr ';'	{printSymbolTable();}
	| TINT varlist ';'	{printf("variaveis int");printSymbolTable();}
	| TFLOAT varlist ';'	{printf("variaveis float");printSymbolTable();}
	;

stmt_list:
	stmt			{printSymbolTable();}
	| stmt_list stmt	{printSymbolTable();}
	;

varlist:
	VAR			{$$ = addNode(VAR, TERMINAL, $1, NULL, NULL, NULL); printSymbolTable();}
	| VAR ',' varlist	{$$ = addNode(VAR, TERMINAL, 0, $1, addOp(COMMA), $3); printSymbolTable();}


expr:
	INT 			{ $$ = addTerminal(CONST, $1); printSymbolTable();}
	| VAR			{ $$ = addNode(VAR, TERMINAL, $1, NULL, NULL, NULL); printSymbolTable();}
	| '-' INT		{ $$ = addTerminal(-$2, 0);  printSymbolTable();}
	| FLOAT 		{ $$ = addTerminal(0, $1); printSymbolTable();}
	| '-' FLOAT 		{ $$ = addTerminal(0, -$2);  printSymbolTable();}
	| expr '*' expr 	{ $$ = addNode(VAR, EXPR, 0, $1, addOp(OPMUL), $3); printSymbolTable();}
	| expr '/' expr 	{ $$ = addNode(VAR, EXPR, 0, $1, addOp(OPDIV), $3); printSymbolTable();}
	| expr '+' expr 	{ $$ = addNode(VAR, EXPR, 0, $1, addOp(OPADD), $3); printSymbolTable();}
	| expr '-' expr 	{ $$ = addNode(VAR, EXPR, 0, $1, addOp(OPSUB), $3); printSymbolTable();}
	| '(' expr ')'		{ $$ = addNode(VAR, EXPR, 0, addOp(OPENP), $2, addOp(CLOSEP)); printSymbolTable();}
	;

%%

void printParseTree(Node *node){
	if(node != NULL){
		switch(node->type){
			case TERMINAL:
  				printf("%d ", node->token.tokenId);

			
			case VAR:
				printf("E\n");
				printParseTree(node->left);
				printParseTree(node->center);
				printParseTree(node->right);
				break;
				
			default:
				printf("%c ",node->type);
		}
	}
}


Node *addNode(int type, int tokenId, int value, Node *left, Node *center, Node *right){
	Node* node = (Node*) malloc(sizeof(Node));

	if(node == NULL){
		printf("Error! ");
		return NULL;
	}

	node->type = type;
	node->token.tokenId = tokenId;
	node->token.value = value;

	node->left = left;
	node->center = center;
	node->right = right;

	return node;
}

Node *addTerminal(int i, int f){
	return	addNode(TERMINAL, i, f, NULL, NULL, NULL);
}

Node *addOp(int op){
	return	addNode(op, op, 0, NULL, NULL, NULL);
}

void yyerror(char *s) {
	fprintf(stderr, "%s\n", s);
}

int main(void) {
	initSymbolTable();
	printSymbolTable();
	yyparse();
	return 0;
}

void initSymbolTable() {
	symbolTable = NULL;
	installReservedWord("if");
	installReservedWord("while");
	installReservedWord("for");
	installReservedWord("int");
	installReservedWord("float");
}

int installSymbol(Symbol *s){
    Symbol *temp;

    if(symbolTable == NULL) {
	symbolTable = s;
	s->id = 1;
    } else {
        temp = symbolTable;
	while (temp->next != NULL) {
		temp = temp->next;
	}
	s->id = temp->id + 1;
	temp->next = s;
	s->next = NULL;
    }

    return s->id;
}

int installReservedWord(char * word)  {
	int id = locateVar(word);
	if(id == -1) {
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char *) malloc(strlen(word) + 1);
		strcpy(s->name, word);
		s->type = RESERVED;
		installSymbol(s);
		return s->id;
	}
	return id;
}

int installVar(char *var) {
	int id = locateVar(var);
	if(id == -1) {
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char *) malloc(strlen(var) + 1);
		strcpy(s->name, var);
		s->type = VAR;
		id = installSymbol(s);	
	}
	return id;
}

int installInt(int v)  {
	Symbol *s = malloc(sizeof(Symbol));
	s->name = (char*) malloc(1);
	s->name[0] = '\0';
	s->type = INT;
	s->v.i = v;
	installSymbol(s);
	return s->id;
}

int installFloat(float f) {
	Symbol *s = malloc(sizeof(Symbol));
	s->name = (char*) malloc(1);
	s->name[0] = '\0';
	s->type = FLOAT;
	s->v.f = f;
	installSymbol(s);
	return s->id;
}

struct Symbol* getSymbol(int key) {
    Symbol *temp;

    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->id == key) {
			return temp;
		}
		temp = temp->next;
	}
    }

    return NULL;
}

int locateVar(char *key) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->type == VAR || temp->type == RESERVED) {
			if(strcmp(temp->name, key) == 0) {
				return temp->id;
			}
		}
		temp = temp->next;
	}
    }
    return -1;
}

void printSymbolTable() {
    Symbol *temp;

    printf("Name ID Type\n");
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		printf("%s\t%d\t%d\n", temp->name, temp->id, temp->type);
		temp = temp->next;
	}
    } else {
	printf("Tabela vazia\n");
    }
}
