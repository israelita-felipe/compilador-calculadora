/*
Author: Vinicius Ferreira de Souza
*/

%{
#include <stdlib.h>
#include "parser.tab.h"
#include "estruturas.h"


void yyerror(char *);


%}

LETTER      [a-zA-Z]
DIGIT       [0-9]
INTEGER     {DIGIT}+
REAL        {INTEGER}\.{INTEGER}
VAR         {LETTER}({LETTER}|{DIGIT}|\_)*


%%
    /* Loops */

"while"	       {
		    yylval.ival = locateVar("while");
		    struct Symbol * s = getSymbol(yylval.ival);
		    return WHILE;
		}

"else"	       {
		    yylval.ival = locateVar("else");
		    struct Symbol * s = getSymbol(yylval.ival);
		    return ELSE;
		}		
		
    
"if"	       {
		    yylval.ival = locateVar("if");
		    struct Symbol * s = getSymbol(yylval.ival);
		    return IF;
		}    
    				
		
   /* Reals  */
{REAL}          {       
                    yylval.ival = installFloat(atof(yytext));
	            struct Symbol * s = getSymbol(yylval.ival);
		    return s->tokenID;                    
                 }

    /* Integers */
{INTEGER}      { 
                    yylval.ival = installInt(atoi(yytext));
		    struct Symbol * s = getSymbol(yylval.ival);
		    return s->tokenID; 
		}
				
"int"	       {
		    yylval.ival = locateVar("int");
		    struct Symbol * s = getSymbol(yylval.ival);
		    return TINT; 
		}
"float"	       {
		    yylval.ival = locateVar("float");
		    struct Symbol * s = getSymbol(yylval.ival);
		    return TFLOAT;
		}

{VAR}	       {
			yylval.ival = installVar(yytext);
			struct Symbol * s = getSymbol(yylval.ival);
			return s->type;
		}
		
	/* Operators */
[-+,=;()^/*}{]		{ return *yytext; }

	/* Skip whitespace */
[ \t\n] 			; 

	/* Anything else is an error */
. 				yyerror("invalid character");

%%

int yywrap(void) {
	return 1;
}






