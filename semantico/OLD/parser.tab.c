/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 5 "parser.y" /* yacc.c:339  */

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "estruturas.h"  


#line 74 "parser.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.tab.h".  */
#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    INT = 258,
    VAR = 259,
    TINT = 260,
    TFLOAT = 261,
    FLOAT = 262,
    WHILE = 263,
    IF = 264,
    ELSE = 265
  };
#endif
/* Tokens.  */
#define INT 258
#define VAR 259
#define TINT 260
#define TFLOAT 261
#define FLOAT 262
#define WHILE 263
#define IF 264
#define ELSE 265

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 13 "parser.y" /* yacc.c:355  */

	int ival;
	float fval;
    struct Node *node;

#line 140 "parser.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 155 "parser.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  29
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   91

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  9
/* YYNRULES -- Number of rules.  */
#define YYNRULES  27
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  56

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   265

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      20,    21,    13,    11,    22,    12,     2,    14,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    16,
       2,    17,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    15,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    18,     2,    19,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    29,    29,    33,    34,    35,    36,    37,    38,    39,
      40,    43,    47,    51,    55,    56,    60,    61,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "INT", "VAR", "TINT", "TFLOAT", "FLOAT",
  "WHILE", "IF", "ELSE", "'+'", "'-'", "'*'", "'/'", "'^'", "';'", "'='",
  "'{'", "'}'", "'('", "')'", "','", "$accept", "program", "stmt",
  "ifelse", "if", "while", "stmt_list", "varlist", "expr", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,    43,    45,    42,    47,    94,    59,    61,   123,   125,
      40,    41,    44
};
# endif

#define YYPACT_NINF -17

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-17)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      53,   -17,   -10,     5,     5,   -17,    -8,    13,    -1,    53,
       1,    45,   -17,   -17,    36,   -17,    53,     6,     1,    26,
      33,    34,     1,     1,   -17,   -17,    35,   -17,    55,   -17,
      53,   -17,     1,     1,     1,     1,   -17,    18,     5,   -17,
     -17,    66,    70,   -17,   -17,   -17,    10,    10,   -17,   -17,
     -17,   -17,    53,    53,   -17,   -17
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    18,    20,     0,     0,    21,     0,     0,     0,     0,
       0,     0,    14,     9,     8,     7,     2,     0,     0,    16,
       0,     0,     0,     0,    19,    22,     0,    20,     0,     1,
       0,    15,     0,     0,     0,     0,     3,     0,     0,     5,
       6,     0,     0,    10,    27,    11,    25,    26,    23,    24,
       4,    17,     0,     0,    13,    12
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -17,   -17,   -16,   -17,   -17,   -17,    42,    -3,    -7
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,    11,    12,    13,    14,    15,    16,    20,    17
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      31,    21,    24,    28,     1,    27,    25,    18,     5,    19,
      31,    37,    22,     8,    45,    41,    42,    32,    33,    34,
      35,    10,    36,    34,    35,    46,    47,    48,    49,    32,
      33,    34,    35,    23,    50,    51,    54,    55,     1,     2,
       3,     4,     5,     6,     7,    29,    30,     8,    38,    39,
      40,    26,     0,     9,    43,    10,     1,     2,     3,     4,
       5,     6,     7,     0,     0,     8,    32,    33,    34,    35,
       0,     9,     0,    10,     0,     0,    44,    32,    33,    34,
      35,    32,    33,    34,    35,     0,     0,    52,     0,     0,
       0,    53
};

static const yytype_int8 yycheck[] =
{
      16,     4,     3,    10,     3,     4,     7,    17,     7,     4,
      26,    18,    20,    12,    30,    22,    23,    11,    12,    13,
      14,    20,    16,    13,    14,    32,    33,    34,    35,    11,
      12,    13,    14,    20,    16,    38,    52,    53,     3,     4,
       5,     6,     7,     8,     9,     0,    10,    12,    22,    16,
      16,     9,    -1,    18,    19,    20,     3,     4,     5,     6,
       7,     8,     9,    -1,    -1,    12,    11,    12,    13,    14,
      -1,    18,    -1,    20,    -1,    -1,    21,    11,    12,    13,
      14,    11,    12,    13,    14,    -1,    -1,    21,    -1,    -1,
      -1,    21
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     6,     7,     8,     9,    12,    18,
      20,    24,    25,    26,    27,    28,    29,    31,    17,     4,
      30,    30,    20,    20,     3,     7,    29,     4,    31,     0,
      10,    25,    11,    12,    13,    14,    16,    31,    22,    16,
      16,    31,    31,    19,    21,    25,    31,    31,    31,    31,
      16,    30,    21,    21,    25,    25
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    23,    24,    25,    25,    25,    25,    25,    25,    25,
      25,    26,    27,    28,    29,    29,    30,    30,    31,    31,
      31,    31,    31,    31,    31,    31,    31,    31
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     4,     3,     3,     1,     1,     1,
       3,     3,     5,     5,     1,     2,     1,     3,     1,     2,
       1,     1,     2,     3,     3,     3,     3,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 29 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, PROGRAM, -1,(yyvsp[0].node),NULL,NULL);printParseTree((yyvsp[0].node));generateCode((yyvsp[0].node));printCodeTable();printSymbolTable();toAssembly();}
#line 1264 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 33 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, STMT, -1, (yyvsp[-1].node), addOp(DEL) , NULL);}
#line 1270 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 34 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, ATTRIB, -1, addTerminal(VAR, (yyvsp[-3].ival)), addOp(OPEQ) , addNode(VAR, STMT, -1, (yyvsp[-1].node), addOp(DEL) , NULL));verifyAttribuition((yyvsp[-3].ival),(yyvsp[-1].node));}
#line 1276 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 35 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, DECL, -1, addTerminal(TINT,(yyvsp[-2].ival)), (yyvsp[-1].node) , addOp(DEL));changeType((yyvsp[-1].node),INT);}
#line 1282 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 36 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, DECL, -1, addTerminal(TFLOAT,(yyvsp[-2].ival)), (yyvsp[-1].node) , addOp(DEL));changeType((yyvsp[-1].node),FLOAT);}
#line 1288 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 37 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, TWHILE, -1, (yyvsp[0].node), NULL , NULL);}
#line 1294 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 38 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, TIF, -1, (yyvsp[0].node), NULL , NULL);}
#line 1300 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 39 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, TELSE, -1, (yyvsp[0].node), NULL , NULL);}
#line 1306 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 40 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, STMT_LIST, -1, addOp(OPENCH), (yyvsp[-1].node) , addOp(CLOSECH));}
#line 1312 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 43 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, TELSE,-1,(yyvsp[-2].node),(yyvsp[0].node),NULL);}
#line 1318 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 47 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR,TIF,-1,(yyvsp[-2].node),(yyvsp[0].node),NULL);}
#line 1324 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 51 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR,TWHILE,-1,(yyvsp[-2].node),(yyvsp[0].node),NULL);}
#line 1330 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 55 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, STMT, -1, (yyvsp[0].node), NULL , NULL);}
#line 1336 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 56 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, STMT_LIST, -1, (yyvsp[-1].node), (yyvsp[0].node) , NULL);}
#line 1342 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 60 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addTerminal(VAR, (yyvsp[0].ival));}
#line 1348 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 61 "parser.y" /* yacc.c:1646  */
    {(yyval.node) = addNode(VAR, VAR_LIST, -1, addTerminal(VAR, (yyvsp[-2].ival)), addOp(COMMA), (yyvsp[0].node));}
#line 1354 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 66 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addTerminal(INT, (yyvsp[0].ival));}
#line 1360 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 67 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addTerminal(INT, (yyvsp[0].ival));  getSymbol((yyvsp[0].ival))->v.i = -1* getSymbol((yyvsp[0].ival))->v.i;}
#line 1366 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 68 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addTerminal(VAR, (yyvsp[0].ival));}
#line 1372 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 69 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addTerminal(FLOAT, (yyvsp[0].ival));}
#line 1378 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 70 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addTerminal(FLOAT, (yyvsp[0].ival)); getSymbol((yyvsp[0].ival))->v.f = -1* getSymbol((yyvsp[0].ival))->v.f;}
#line 1384 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 71 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addNode(VAR, EXPR, -1, (yyvsp[-2].node), addOp(OPMUL), (yyvsp[0].node));}
#line 1390 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 72 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addNode(VAR, EXPR, -1, (yyvsp[-2].node), addOp(OPDIV), (yyvsp[0].node));}
#line 1396 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 73 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addNode(VAR, EXPR, -1, (yyvsp[-2].node), addOp(OPADD), (yyvsp[0].node));}
#line 1402 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 74 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addNode(VAR, EXPR, -1, (yyvsp[-2].node), addOp(OPSUB), (yyvsp[0].node));}
#line 1408 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 75 "parser.y" /* yacc.c:1646  */
    { (yyval.node) = addNode(VAR, EXPR, -1, addOp(OPENP), (yyvsp[-1].node), addOp(CLOSEP));}
#line 1414 "parser.tab.c" /* yacc.c:1646  */
    break;


#line 1418 "parser.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 78 "parser.y" /* yacc.c:1906  */


int tempID = 0; 

void printFooterTable(){
    printf("\n--------------------------------------------------\n");
} 

int getID(){
    	char* str;
    	asprintf (&str, "t%i", ++tempID);
    	return installVar(str);
}
char* getLabel(){
	char* str;
	asprintf(&str,"label%i",++tempID);
	return str;
}

int generateCode(Node *node){
		if(node != NULL){
		char* labelCondition;
		char* labelJal;
		char* labelElse;
		switch(node->type){
			case TERMINAL:

				switch(node->token.tokenId){
					case INT:
						return node->token.value;
						break;
					case FLOAT:
						return node->token.value;
						break;
					case VAR:
						return node->token.value;
						break;					
					case EXPR:
						if(node->center->token.tokenId==EXPR){
							return installCode(getID(),generateCode(node->center),-1,OPEQ);
						}
						return installCode(getID(),generateCode(node->left),generateCode(node->right),generateCode(node->center));
						break;													
				}
				break;
			
			case VAR:	
				switch(node->token.tokenId){
					case ATTRIB:
						return installCode(generateCode(node->left),generateCode(node->right),-1,OPEQ);
						break;
					case STMT:
						return generateCode(node->left);
						break;
					case STMT_LIST:
						generateCode(node->left);
						generateCode(node->center);
						break;			
					case EXPR:
						if(node->center->token.tokenId==EXPR){
							return installCode(getID(),generateCode(node->center),-1,OPEQ);
						}
						return installCode(getID(),generateCode(node->left),generateCode(node->right),generateCode(node->center));
						break;
						
					case TWHILE:
						if(node->center==NULL){
							generateCode(node->left);
						}else{
							// gera dois label's para goto
							labelCondition = getLabel();
							labelJal = getLabel();
							installLabel(labelCondition);
							// instala o while com a expressao a esquerda
							installCondition(generateCode(node->left),installInt(0),labelJal);
							// gera a parte de dentro do while
							generateCode(node->center);
							// gera label goto de volta ao while
							installGoto(labelCondition);
							// gera label apos while
							installLabel(labelJal);
						}
						break;												
					case TIF:
						if(node->center==NULL){
							generateCode(node->left);
						}else{
							// gera dois label's para goto
							labelCondition = getLabel();
							labelJal = getLabel();
							installLabel(labelCondition);
							// instala o if com a expressao a esquerda
							installCondition(generateCode(node->left),installInt(0),labelJal);
							// gera a parte de dentro do while
							generateCode(node->center);							
							// gera label apos while
							installLabel(labelJal);
						}
						break;
					case TELSE:
						if(node->center==NULL){
							generateCode(node->left);
						}else{
							// gera dois label's para goto
							labelCondition = getLabel();
							labelJal = getLabel();
							labelElse = getLabel();
							
							installLabel(labelCondition);
							// instala o if com a expressao a esquerda
							installCondition(generateCode(node->left->left),installInt(0),labelJal);
							// gera a parte de dentro do else
							generateCode(node->left->center);							
							// pula o bloco de else
							installGoto(labelElse);
							// gera label apos else
							installLabel(labelJal);
							generateCode(node->center);
							// final do else
							installLabel(labelElse);
						}
						break;
				}			
				break;
			case OPMUL:
				return OPMUL;
				break;
			case OPDIV:
				return OPDIV;
				break;
			case OPADD:
				return OPADD;
				break;
			case OPSUB:
				return OPSUB;
				break;										
		}
	}
	return -1;
}

int verifyType(){
	Symbol *temp;

	if(symbolTable != NULL) {
	    temp = symbolTable;
		while (temp != NULL) {
			if(temp->tokenID==0){
				if(temp->type!=RESERVED){
					printf("# Símbolo não declarado <%d,%d>\n",temp->type,temp->id);
				}
			}
			temp=temp->next;
		}
	}
}

void verifyAttribuition(int id,Node *node){
	verifyType();
	Symbol *s = getSymbol(id);	
	Symbol *n = getSymbol(node->token.value);

	if(s!=NULL&&n!=NULL){
	  if(s->tokenID<n->tokenID){
		  printf("# Erro de atribuição: %d recebe %d\n",s->tokenID,n->tokenID);				
	  }
	}		
}

void changeType(Node *node, int dataType){
	if(node!=NULL){
		Symbol* s = getSymbol(node->token.value);
		if(s!=NULL){
            		if(s->tokenID>0){
                		printf("# Variável já declarada <%d,%d>\n",s->type,s->id);
            	}else{
		    s->tokenID=dataType;			    
            	}
	    }
	    changeType(node->left, dataType);
	    changeType(node->center, dataType);
	    changeType(node->right, dataType);
	}
}
void printParseTree(Node *node){
	if(node != NULL){
		switch(node->type){
			case TERMINAL:

  				printf("<%d,%d>", node->token.tokenId, node->token.value);
				break;
			
			case VAR:
				printParseTree(node->left);
				printParseTree(node->center);
				printParseTree(node->right);
				break;
				
			default:
				printf("<%d>",node->type);
				break;
		}
	}
}

void print(Code *code){
	
	if(code!=NULL){
		if(code->t!=0){
			printf("%d\t",code->t);
			printf("=\t");
		}else{
			printf("\t\t");
		}
		Symbol *s = NULL;
		if(code->op==0){
			printf("\t\t\t%s\n",code->label);
		}
		else if(code->op==OPEQ){
			printf("%d\n",code->e);					
		}else{
			if(code->op!=OPGOTO){
				printf("%d\t",code->e);
			}else{
				printf("\t");
			}
			switch(code->op){
				    case OPMUL:
					printf("*\t");
					break;

				    case OPADD:
					printf("+\t");
					break;

				    case OPSUB:
					printf("-\t");
					break;

				    case OPDIV:
					printf("/\t");
					break;		
				    case OPLEQ:
				    	printf("BEQ\t");
				    	break;
				    case OPGOTO:
				    	printf("GOTO\t");
				    	break;		    
			}		
			if(code->op!=OPGOTO){
				printf("%d\t",code->d);							
			}else{
				printf("\t");
			}
			if(code->label!=NULL){
				printf("%s\n",code->label);
			}else{
				printf("\n");
			}
		}		
		print(code->next);		
	}
}
void printCodeTable(){
	printFooterTable();
	printf("-                   Code Table                   -");
	printFooterTable();
	printf("ID\t-\tL\tOp.\tR\tLabel\n");
	if(codeTable==NULL){
		printf("Empty Table");
	}
	Code * code = codeTable;
	print(code);
	printFooterTable();
}

Node *addNode(int type, int tokenId, int value, Node *left, Node *center, Node *right){
	Node* node = (Node*) malloc(sizeof(Node));

	if(node == NULL){
		printf("Error! ");
		return NULL;
	}

	node->type = type;
	node->token.tokenId = tokenId;
	node->token.value = value;

	node->left = left;
	node->center = center;
	node->right = right;

	return node;
}

Node *addTerminal(int i, int f){
	return	addNode(TERMINAL, i, f, NULL, NULL, NULL);
}

Node *addOp(int op){
	return	addNode(op, op, -1, NULL, NULL, NULL);
}

void yyerror(char *s) {
	fprintf(stderr, "%s\n", s);
}

int main(void) {
	initSymbolTable();
	printSymbolTable();
	yyparse();
	return 0;
}

void initSymbolTable() {
	symbolTable = NULL;
	installReservedWord("if");
	installReservedWord("else");
	installReservedWord("while");
	installReservedWord("for");
	installReservedWord("int");
	installReservedWord("float");
}

int installSymbol(Symbol *s){
    Symbol *temp;

    if(symbolTable == NULL) {
	symbolTable = s;
	s->id = 1;
    } else {
        temp = symbolTable;
	while (temp->next != NULL) {
		temp = temp->next;
	}
	s->id = temp->id + 1;
	temp->next = s;
	s->next = NULL;
    }	
    return s->id;
}

int installReservedWord(char * word)  {
	int id = locateVar(word);
	if(id == -1) {
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char *) malloc(strlen(word) + 1);
		strcpy(s->name, word);
		s->type = RESERVED;
		installSymbol(s);
		return s->id;
	}
	return id;
}

int installVar(char *var) {
	int id = locateVar(var);
	if(id == -1) {
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char *) malloc(strlen(var) + 1);
		strcpy(s->name, var);
		s->type = VAR;
		id = installSymbol(s);	
	}
	return id;
}

int installInt(int v)  {	
	int id = locateInt(v);
	if(id==-1)
	{
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char*) malloc(1);
		s->name[0] = '\0';
		s->type = TINT;
		s->tokenID=INT;
		s->v.i = v;
		installSymbol(s);
		return s->id;
	}
	return id;
}

int installFloat(float f) {
	int id = locateFloat(f);
	if(id==-1)
	{
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char*) malloc(1);
		s->name[0] = '\0';
		s->type = TFLOAT;
		s->tokenID=FLOAT;
		s->v.f = f;
		installSymbol(s);
		return s->id;
	}
	return id;
}

struct Symbol* getSymbol(int key) {
    Symbol *temp;

    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->id == key) {
			return temp;
		}
		temp = temp->next;
	}
    }

    return NULL;
}

int locateVar(char *key) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->type == VAR || temp->type == RESERVED) {
			if(strcmp(temp->name, key) == 0) {
				return temp->id;
			}
		}
		temp = temp->next;
	}
    }
    return -1;
}

int installCode(int t, int e, int d, int op){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->t = t;
	code->e = e;
	code->d = d;
	code->op = op;

	Symbol *st = getSymbol(t);
	Symbol *se = getSymbol(e);
	Symbol *sd = getSymbol(d);

	if(st!=NULL && st->tokenID==0){
		int id_se,id_sd = 0;
		if(se!=NULL && se->tokenID>=sd->tokenID){
			st->tokenID = se->tokenID;
		}else if(sd!=NULL){
			st->tokenID = sd->tokenID;
		}
	}else if(st!=NULL){
		if(se!=NULL && se->tokenID==0){
			se->tokenID=st->tokenID;
		}
		if(sd!=NULL && sd->tokenID==0){
			sd->tokenID=st->tokenID;
		}
	}

	if(codeTable==NULL){
		codeTable=code;
		return t;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
	return t;
}

void installCondition(int e,int d,char *label){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->e = e;
	code->d = d;
	code->op = OPLEQ;
	code->label = (char*) malloc(sizeof(label));
	code->label = label;	

	if(codeTable==NULL){
		codeTable=code;
		return;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
}

void installGoto(char *label){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->op = OPGOTO;
	code->label = (char*) malloc(sizeof(label));
	code->label = label;	

	if(codeTable==NULL){
		codeTable=code;
		return;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
}

void installLabel(char *label){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->label = (char*) malloc(sizeof(label));
	code->label = label;	

	if(codeTable==NULL){
		codeTable=code;
		return;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
}

int locateInt(int value) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->v.i==value&&temp->type==INT) {
		     return temp->id;
		}
		temp = temp->next;
	}
    }
    return -1;
}

int locateFloat(float value) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->v.f==value&&temp->type==FLOAT) {			
		     return temp->id;			
		}
		temp = temp->next;
	}
    }
    return -1;
}

void printHeadTable(){
    printFooterTable();
    printf("-                  Symbol Table                  -");
    printFooterTable();
    printf("Name\tID\tType\tData_type\tValue\n");
}

void printSymbolTable() {
    Symbol *temp;
    printHeadTable();
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		printf("%s\t%d\t%d\t%d\t", temp->name, temp->id, temp->type,temp->tokenID);
		if(temp->type == TFLOAT){
			printf("\t%f\n",temp->v.f);
		}
		else if(temp->type==TINT){
			printf("\t%d\n",temp->v.i);			
		}else{
			printf("\n");		
		}			
		temp = temp->next;
	}
    } else {
	printf("Empty Table");
    }
    printFooterTable();
}

void toAssembly(){
	createData();
	createText();
}
void createData(){
	printf(".data\n");
	Symbol *s = symbolTable;
	while(s!=NULL){
		if(s->type == VAR){
			printf("\t%s:\t.word\n",s->name);
		}
		s = s->next;		
	}
}

void createText(){
	printf(".text\n");
	printf(".glob main\n");
	printf("main: \n");
	Code *c = codeTable;
	text(c);
}

void text(Code *code){
	if(code!=NULL){
	
		Symbol *valor = NULL;
		Symbol *esquerda = NULL;
		Symbol *direita = NULL;
		
		if(code->t!=0){
			valor = getSymbol(code->t);
		}
		
		if(code->op==0){
			printf("%s:\n",code->label);
		}else if(code->op==OPEQ){
			esquerda = getSymbol(code->e);
			if(esquerda->tokenID==INT&&esquerda->type!=VAR){
				printf("\tli $s0, %d\n",esquerda->v.i);
				printf("\tsw $s0, %s($0)\n",valor->name);					
			}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
				printf("\tli $s0, %f\n",esquerda->v.f);
				printf("\tsw $s0, %s($0)\n",valor->name);					
			}else{
				printf("\tlw $s3, %s($0)\n",esquerda->name);
				printf("\tsw $s3, %s($0)\n",valor->name);
			}			
		}else{			
			
			esquerda = getSymbol(code->e);
			direita = getSymbol(code->d);						
			
			switch(code->op){
				    case OPMUL:
				    
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}
					
					printf("\tmult $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;

				    case OPADD:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}
					
					printf("\tadd $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;

				    case OPSUB:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}

					
					printf("\tsub $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;

				    case OPDIV:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}

					
					printf("\tdiv $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;		
				    case OPLEQ:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}					
					
					printf("\tli $s1, 0\n");				    	
				    	printf("\tbeq $0, $s1, %s\n",code->label);
				    	break;
				    case OPGOTO:
				    	if(code->label!=NULL){
						printf("\tjal %s\n",code->label);
					}
				    	break;		    
			}											
		}		
		text(code->next);		
	}
}

