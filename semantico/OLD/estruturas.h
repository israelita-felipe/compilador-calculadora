#ifndef ESTRUTURAS_H
#define ESTRUTURAS_H

    #define TERMINAL 	0
    #define EXPR 	2
    #define RESERVED 	3
    #define STMT	4
    #define VAR_LIST	5
    #define PROGRAM	6
    #define STMT_LIST   7
    #define ATTRIB	8
    #define DECL	9
 
    #define OPENP 	40
    #define CLOSEP 	41
    #define OPMUL 	42
    #define OPADD 	43
    #define OPSUB 	45
    #define OPDIV 	47
    #define OPEQ	51
    #define COMMA 	48
    #define OPENCH	52
    #define CLOSECH	53
    #define OPLEQ	61
    #define OPGOTO	62

    #define CONST 	49
    #define DEL		50   
    
    #define TWHILE	60
    #define TIF		63
    #define TELSE	64
    
    typedef struct Code{
		int t;
		int e;
		int d;
		int op;
		char* label;   	
		struct Code *next;
    } Code;
    
    struct Code *codeTable;

    typedef struct Token{
        int tokenId;
	int value;
    } Token;

    typedef struct Node{
	int type;
	struct Token token;
    	struct Node *left, *center, *right;
    }Node;

    typedef union Value{
        int i;
        float f;
        char *str;
    }Value;

    typedef struct Symbol{
	int id;
	char *name;
    	int tokenID;
    	int type;
    	Value v;
	struct Symbol *next;
    } Symbol;

    struct Symbol *symbolTable;

    void initSymbolTable();

	int yylex(void);
	void yyerror(char *);
	void printParseTree(Node* node);

	Node *addNode(int type, int tokenId, int value, Node *left, Node *center, Node *right);
	Node *addTerminal(int i, int f);
	Node *addOp(int op);


	int locateVar(char *var);
	int locateInt(int value);
	int locateFloat(float value);

	int installReservedWord(char * word) ;
	int installVar(char *var);
	
	int generateCode(Node *node);
	void printCodeTable();
	struct Symbol* getSymbol(int key);

	void printSymbolTable();

	int installInt(int v);
	int installFloat(float f) ;		

	// analisador sermantico

	// verificacao de declaracao
	void declaration(Node *node);
	// trocando o tipo
	void changeType(Node *node, int dataType);
	// verifica o tipo de atribuicao
	void verifyAttribuition(int id,Node *node);
		
	int installCode(int t, int e, int d, int op);
	void installCondition(int e,int d,char* label);
	void installGoto(char* label);
	void installLabel(char* label);
	
	void toAssembly();
	void createData();
	void createText();
	void text();

#endif
