/*
Author: Vinicius Ferreira de Souza
*/

%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "estruturas.h"  

%}

%union{
	int ival;
	float fval;
    struct Node *node;
}

%token<ival> INT VAR TINT TFLOAT FLOAT WHILE IF ELSE
%token<fval> 
%type<node> expr varlist stmt stmt_list program while if

%left '+' '-'
%left '*' '/'
%left '^'

%%
program:
	stmt_list 		{$$ = addNode(VAR, PROGRAM, -1,$1,NULL,NULL);
				 printParseTree($1);
				 generateCode($1,NULL);
				 printCodeTable();
				 printSymbolTable();
				 toAssembly();
				 }
	
	; 

stmt:
	expr ';'		{$$ = addNode(VAR, STMT, -1, $1, addOp(DEL) , NULL);}
	| VAR '=' expr ';'	{$$ = addNode(VAR, ATTRIB, -1, addTerminal(VAR, $1), addOp(OPEQ) , addNode(VAR, STMT, -1, $3, addOp(DEL) , NULL));verifyAttribuition($1,$3);}
	| TINT varlist ';'	{$$ = addNode(VAR, DECL, -1, addTerminal(TINT,$1), $2 , addOp(DEL));changeType($2,INT);}
	| TFLOAT varlist ';'	{$$ = addNode(VAR, DECL, -1, addTerminal(TFLOAT,$1), $2 , addOp(DEL));changeType($2,FLOAT);}
	| while			{$$ = addNode(VAR, TWHILE, -1, $1, NULL , NULL);}
	| if			{$$ = addNode(VAR, TIF, -1, $1, NULL , NULL);}
	|'{'stmt_list'}'	{$$ = addNode(VAR, STMT_LIST, -1, addOp(OPENCH), $2 , addOp(CLOSECH));}	
	;

if:
	IF '(' expr ')' stmt ELSE stmt	{$$ = addNode(VAR,TIF,-1,$3,$5,addNode(VAR,TELSE,-1,$7,NULL,NULL));}
	| IF '(' expr ')' stmt		{$$ = addNode(VAR,TIF,-1,$3,$5,NULL);}
	;

while:
	WHILE '(' expr ')' stmt_list	{$$ = addNode(VAR,TWHILE,-1,$3,$5,NULL);}
	;
			
stmt_list:
	stmt			{$$ = addNode(VAR, STMT, -1, $1, NULL , NULL);}
	| stmt_list stmt	{$$ = addNode(VAR, STMT_LIST, -1, $1, $2 , NULL);}
	;

varlist:
	VAR			{$$ = addTerminal(VAR, $1);}
	| VAR ',' varlist	{$$ = addNode(VAR, VAR_LIST, -1, addTerminal(VAR, $1), addOp(COMMA), $3);}
	;


expr:
	INT 			{ $$ = addTerminal(INT, $1);}
	| '-' INT 		{ $$ = addTerminal(INT, $2);  getSymbol($2)->v.i = -1* getSymbol($2)->v.i;}	
	| VAR			{ $$ = addTerminal(VAR, $1);}
	| FLOAT 		{ $$ = addTerminal(FLOAT, $1);}
	| '-' FLOAT 		{ $$ = addTerminal(FLOAT, $2); getSymbol($2)->v.f = -1* getSymbol($2)->v.f;}	
	| expr '*' expr 	{ $$ = addNode(VAR, EXPR, -1, $1, addOp(OPMUL), $3);}
	| expr '/' expr 	{ $$ = addNode(VAR, EXPR, -1, $1, addOp(OPDIV), $3);}
	| expr '+' expr 	{ $$ = addNode(VAR, EXPR, -1, $1, addOp(OPADD), $3);}
	| expr '-' expr 	{ $$ = addNode(VAR, EXPR, -1, $1, addOp(OPSUB), $3);}
	| '(' expr ')'		{ $$ = addNode(VAR, EXPR, -1, addOp(OPENP), $2, addOp(CLOSEP));}	
	;

%%

int tempID = 0; 

void printFooterTable(){
    printf("\n--------------------------------------------------\n");
} 

int getID(){
    	char* str;
    	asprintf (&str, "t%i", ++tempID);
    	return installVar(str);
}
char* getLabel(){
	char* str;
	asprintf(&str,"label%i",++tempID);
	return str;
}

int generateCode(Node *node,char* labelElse){
		int id = -1;
		if(node != NULL){
		char* labelCondition;
		char* labelJal;
		switch(node->type){
			case TERMINAL:

				switch(node->token.tokenId){
					case INT:
						return node->token.value;
						break;
					case FLOAT:
						return node->token.value;
						break;
					case VAR:
						return node->token.value;
						break;					
					case EXPR:
						if(node->center->token.tokenId==EXPR){
							return installCode(getID(),generateCode(node->center,NULL),-1,OPEQ);
						}
						return installCode(getID(),generateCode(node->left,NULL),generateCode(node->right,NULL),generateCode(node->center,NULL));
						break;													
				}
				break;
			
			case VAR:	
				switch(node->token.tokenId){
					case ATTRIB:
						return installCode(generateCode(node->left,NULL),generateCode(node->right,NULL),-1,OPEQ);
						break;
					case STMT:
						return generateCode(node->left,NULL);
						break;
					case STMT_LIST:
						generateCode(node->left,NULL);
						generateCode(node->center,NULL);
						break;			
					case EXPR:
						if(node->center->token.tokenId==EXPR){
							return installCode(getID(),generateCode(node->center,NULL),-1,OPEQ);
						}
						return installCode(getID(),generateCode(node->left,NULL),generateCode(node->right,NULL),generateCode(node->center,NULL));
						break;
						
					case TWHILE:						
						// gera dois label's para goto
						labelCondition = getLabel();
						labelJal = getLabel();
						installLabel(labelCondition);
						// instala o while com a expressao a esquerda
						installCondition(generateCode(node->left,NULL),installInt(0),labelJal);
						// gera a parte de dentro do while
						generateCode(node->center,NULL);
						// gera label goto de volta ao while
						installGoto(labelCondition);
						// gera label apos while
						installLabel(labelJal);						
						break;												
					case TIF:						
						// gera dois label's para goto
						labelCondition = getLabel();
						labelJal = getLabel();
						
						//installLabel(labelCondition);						
						// instala o if com a expressao a esquerda
						installCondition(generateCode(node->left,NULL),installInt(0),labelJal);
						// gera a parte de dentro do while
						generateCode(node->center,NULL);							
						// gera label apos while
						char* labelElseGoTo = getLabel();
						installGoto(labelElseGoTo);
						installLabel(labelJal);	
						// se tiver else					
						generateCode(node->right,labelElseGoTo);
						break;
					case TELSE:						
												
						// gera a parte de dentro
						generateCode(node->left,NULL);							
						// pula o bloco de else
						installLabel(labelElse);
						break;
				}			
				break;
			case OPMUL:
				return OPMUL;
				break;
			case OPDIV:
				return OPDIV;
				break;
			case OPADD:
				return OPADD;
				break;
			case OPSUB:
				return OPSUB;
				break;										
		}
	}
	return id;
}

int verifyType(){
	Symbol *temp;

	if(symbolTable != NULL) {
	    temp = symbolTable;
		while (temp != NULL) {
			if(temp->tokenID==0){
				if(temp->type!=RESERVED){
					printf("# Símbolo não declarado <%d,%d>\n",temp->type,temp->id);
				}
			}
			temp=temp->next;
		}
	}
}

void verifyAttribuition(int id,Node *node){
	verifyType();
	Symbol *s = getSymbol(id);	
	Symbol *n = getSymbol(node->token.value);

	if(s!=NULL&&n!=NULL){
	  if(s->tokenID<n->tokenID){
		  printf("# Erro de atribuição: %d recebe %d\n",s->tokenID,n->tokenID);				
	  }
	}		
}

void changeType(Node *node, int dataType){
	if(node!=NULL){
		Symbol* s = getSymbol(node->token.value);
		if(s!=NULL){
            		if(s->tokenID>0){
                		printf("# Variável já declarada <%d,%d>\n",s->type,s->id);
            	}else{
		    s->tokenID=dataType;			    
            	}
	    }
	    changeType(node->left, dataType);
	    changeType(node->center, dataType);
	    changeType(node->right, dataType);
	}
}
void printParseTree(Node *node){
	if(node != NULL){
		switch(node->type){
			case TERMINAL:

  				printf("<%d,%d>", node->token.tokenId, node->token.value);
				break;
			
			case VAR:
				printParseTree(node->left);
				printParseTree(node->center);
				printParseTree(node->right);
				break;
				
			default:
				printf("<%d>",node->type);
				break;
		}
	}
}

void print(Code *code){
	
	if(code!=NULL){
		if(code->t!=0){
			printf("%d\t",code->t);
			printf("=\t");
		}else{
			printf("\t\t");
		}
		Symbol *s = NULL;
		if(code->op==0){
			printf("\t\t\t%s\n",code->label);
		}
		else if(code->op==OPEQ){
			printf("%d\n",code->e);					
		}else{
			if(code->op!=OPGOTO){
				printf("%d\t",code->e);
			}else{
				printf("\t");
			}
			switch(code->op){
				    case OPMUL:
					printf("*\t");
					break;

				    case OPADD:
					printf("+\t");
					break;

				    case OPSUB:
					printf("-\t");
					break;

				    case OPDIV:
					printf("/\t");
					break;		
				    case OPLEQ:
				    	printf("BEQ\t");
				    	break;
				    case OPGOTO:
				    	printf("GOTO\t");
				    	break;		    
			}		
			if(code->op!=OPGOTO){
				printf("%d\t",code->d);							
			}else{
				printf("\t");
			}
			if(code->label!=NULL){
				printf("%s\n",code->label);
			}else{
				printf("\n");
			}
		}		
		print(code->next);		
	}
}
void printCodeTable(){
	printFooterTable();
	printf("-                   Code Table                   -");
	printFooterTable();
	printf("ID\t-\tL\tOp.\tR\tLabel\n");
	if(codeTable==NULL){
		printf("Empty Table");
	}
	Code * code = codeTable;
	print(code);
	printFooterTable();
}

Node *addNode(int type, int tokenId, int value, Node *left, Node *center, Node *right){
	Node* node = (Node*) malloc(sizeof(Node));

	if(node == NULL){
		printf("Error! ");
		return NULL;
	}

	node->type = type;
	node->token.tokenId = tokenId;
	node->token.value = value;

	node->left = left;
	node->center = center;
	node->right = right;

	return node;
}

Node *addTerminal(int i, int f){
	return	addNode(TERMINAL, i, f, NULL, NULL, NULL);
}

Node *addOp(int op){
	return	addNode(op, op, -1, NULL, NULL, NULL);
}

void yyerror(char *s) {
	fprintf(stderr, "%s\n", s);
}

int main(void) {
	initSymbolTable();
	printSymbolTable();
	yyparse();
	return 0;
}

void initSymbolTable() {
	symbolTable = NULL;
	installReservedWord("if");
	installReservedWord("else");
	installReservedWord("while");
	installReservedWord("for");
	installReservedWord("int");
	installReservedWord("float");
}

int installSymbol(Symbol *s){
    Symbol *temp;

    if(symbolTable == NULL) {
	symbolTable = s;
	s->id = 1;
    } else {
        temp = symbolTable;
	while (temp->next != NULL) {
		temp = temp->next;
	}
	s->id = temp->id + 1;
	temp->next = s;
	s->next = NULL;
    }	
    return s->id;
}

int installReservedWord(char * word)  {
	int id = locateVar(word);
	if(id == -1) {
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char *) malloc(strlen(word) + 1);
		strcpy(s->name, word);
		s->type = RESERVED;
		installSymbol(s);
		return s->id;
	}
	return id;
}

int installVar(char *var) {
	int id = locateVar(var);
	if(id == -1) {
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char *) malloc(strlen(var) + 1);
		strcpy(s->name, var);
		s->type = VAR;
		id = installSymbol(s);	
	}
	return id;
}

int installInt(int v)  {	
	int id = locateInt(v);
	if(id==-1)
	{
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char*) malloc(1);
		s->name[0] = '\0';
		s->type = TINT;
		s->tokenID=INT;
		s->v.i = v;
		installSymbol(s);
		return s->id;
	}
	return id;
}

int installFloat(float f) {
	int id = locateFloat(f);
	if(id==-1)
	{
		Symbol *s = malloc(sizeof(Symbol));
		s->name = (char*) malloc(1);
		s->name[0] = '\0';
		s->type = TFLOAT;
		s->tokenID=FLOAT;
		s->v.f = f;
		installSymbol(s);
		return s->id;
	}
	return id;
}

struct Symbol* getSymbol(int key) {
    Symbol *temp;

    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->id == key) {
			return temp;
		}
		temp = temp->next;
	}
    }

    return NULL;
}

int locateVar(char *key) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->type == VAR || temp->type == RESERVED) {
			if(strcmp(temp->name, key) == 0) {
				return temp->id;
			}
		}
		temp = temp->next;
	}
    }
    return -1;
}

int installCode(int t, int e, int d, int op){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->t = t;
	code->e = e;
	code->d = d;
	code->op = op;

	Symbol *st = getSymbol(t);
	Symbol *se = getSymbol(e);
	Symbol *sd = getSymbol(d);

	if(st!=NULL && st->tokenID==0){
		int id_se,id_sd = 0;
		if(se!=NULL && se->tokenID>=sd->tokenID){
			st->tokenID = se->tokenID;
		}else if(sd!=NULL){
			st->tokenID = sd->tokenID;
		}
	}else if(st!=NULL){
		if(se!=NULL && se->tokenID==0){
			se->tokenID=st->tokenID;
		}
		if(sd!=NULL && sd->tokenID==0){
			sd->tokenID=st->tokenID;
		}
	}

	if(codeTable==NULL){
		codeTable=code;
		return t;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
	return t;
}

void installCondition(int e,int d,char *label){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->e = e;
	code->d = d;
	code->op = OPLEQ;
	code->label = (char*) malloc(sizeof(label));
	code->label = label;	

	if(codeTable==NULL){
		codeTable=code;
		return;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
}

void installGoto(char *label){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->op = OPGOTO;
	code->label = (char*) malloc(sizeof(label));
	code->label = label;	

	if(codeTable==NULL){
		codeTable=code;
		return;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
}

void installLabel(char *label){
	Code *code = (Code*)(malloc(sizeof(Code)));
	code->label = (char*) malloc(sizeof(label));
	code->label = label;	

	if(codeTable==NULL){
		codeTable=code;
		return;
	}
	Code *tempCode = codeTable;
	while(tempCode->next!=NULL){
		tempCode = tempCode->next;
	}
	tempCode->next = code;
}

int locateInt(int value) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->v.i==value) {
		     return temp->id;
		}
		temp = temp->next;
	}
    }
    return -1;
}

int locateFloat(float value) {
    Symbol *temp;
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		if(temp->v.f==value) {			
		     return temp->id;			
		}
		temp = temp->next;
	}
    }
    return -1;
}

void printHeadTable(){
    printFooterTable();
    printf("-                  Symbol Table                  -");
    printFooterTable();
    printf("Name\tID\tType\tData_type\tValue\n");
}

void printSymbolTable() {
    Symbol *temp;
    printHeadTable();
    if(symbolTable != NULL) {
        temp = symbolTable;
	while (temp != NULL) {
		printf("%s\t%d\t%d\t%d\t", temp->name, temp->id, temp->type,temp->tokenID);
		if(temp->type == TFLOAT){
			printf("\t%f\n",temp->v.f);
		}
		else if(temp->type==TINT){
			printf("\t%d\n",temp->v.i);			
		}else{
			printf("\n");		
		}			
		temp = temp->next;
	}
    } else {
	printf("Empty Table");
    }
    printFooterTable();
}

void toAssembly(){
	createData();
	createText();
}
void createData(){
	printf(".data\n");
	Symbol *s = symbolTable;
	while(s!=NULL){
		if(s->type == VAR){
			printf("\t%s:\t.word\n",s->name);
		}
		s = s->next;		
	}
}

void createText(){
	printf(".text\n");
	printf(".glob main\n");
	printf("main: \n");
	Code *c = codeTable;
	text(c);
}

void text(Code *code){
	if(code!=NULL){
	
		Symbol *valor = NULL;
		Symbol *esquerda = NULL;
		Symbol *direita = NULL;
		
		if(code->t!=0){
			valor = getSymbol(code->t);
		}
		
		if(code->op==0){
			printf("%s:\n",code->label);
		}else if(code->op==OPEQ){
			esquerda = getSymbol(code->e);
			if(esquerda->tokenID==INT&&esquerda->type!=VAR){
				printf("\tli $s0, %d\n",esquerda->v.i);
				printf("\tsw $s0, %s($0)\n",valor->name);					
			}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
				printf("\tli $s0, %f\n",esquerda->v.f);
				printf("\tsw $s0, %s($0)\n",valor->name);					
			}else{
				printf("\tlw $s3, %s($0)\n",esquerda->name);
				printf("\tsw $s3, %s($0)\n",valor->name);
			}			
		}else{			
			
			esquerda = getSymbol(code->e);
			direita = getSymbol(code->d);						
			
			switch(code->op){
				    case OPMUL:
				    
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}
					
					printf("\tmult $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;

				    case OPADD:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}
					
					printf("\tadd $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;

				    case OPSUB:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}

					
					printf("\tsub $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;

				    case OPDIV:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}
					
					if(direita->tokenID==INT&&direita->type!=VAR){
						printf("\tli $s1, %d\n",direita->v.i);
					}else if(direita->tokenID==FLOAT&&direita->type!=VAR){
						printf("\tli $s1, %f\n",direita->v.f);
					}else{
						printf("\tlw $s1, %s($0)\n",direita->name);
					}

					
					printf("\tdiv $s3, $s0, $s1\n");
					printf("\tsw $s3, %s($0)\n",valor->name);
					break;		
				    case OPLEQ:
					
					if(esquerda->tokenID==INT&&esquerda->type!=VAR){
						printf("\tli $s0, %d\n",esquerda->v.i);
					}else if(esquerda->tokenID==FLOAT&&esquerda->type!=VAR){
						printf("\tli $s0, %f\n",esquerda->v.f);
					}else{
						printf("\tlw $s0, %s($0)\n",esquerda->name);
					}					
					
					printf("\tli $s1, 0\n");				    	
				    	printf("\tbeq $0, $s1, %s\n",code->label);
				    	break;
				    case OPGOTO:
				    	if(code->label!=NULL){
						printf("\tjal %s\n",code->label);
					}
				    	break;		    
			}											
		}		
		text(code->next);		
	}
}

