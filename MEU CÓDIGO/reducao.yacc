%{
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "tbl_symbol.h"
#include "syntatic_tree.h"

int TERMINAL = 0;
int VARIAVEL = 1;
int OPERADOR = 2;


typedef struct NO
{
    int tipo;
    float valor;
    struct NO *LEFT, *CENTER, *RIGHT;
} NO;

NO* root = NULL;

NO* newNode(int tipo, float valor, NO* LEFT, NO* CENTER, NO* RIGHT);

void printNO(NO* node)
{    
	if(node->tipo==OPERADOR)
	{
	     printf("%c",(int)node->valor);        	
	}else if(node->tipo==TERMINAL){
	     printf("%.2f",node->valor);        	     
	}   
}

void print(NO* node)
{
	if(node!=NULL)
	{
        printNO(node);	   
	    print(node->LEFT);
	    print(node->CENTER);
	    print(node->RIGHT);
	}
}

float calcula(NO* node)
{
    float valor = 0.0;
	if(node==NULL) return;
	if(node->tipo==TERMINAL)
	{
		return (float)node->valor;
	}
	if(node->CENTER->tipo==OPERADOR){
		switch((int)node->CENTER->valor)
		{
			case '-':
				return (float)calcula(node->LEFT)-calcula(node->RIGHT);
				break;
			case '+':
				return (float)calcula(node->LEFT)+calcula(node->RIGHT);
				break;
			case '/':			    
			    valor =  calcula(node->RIGHT);
				if(valor!=0.0)
				{
				    return (float)calcula(node->LEFT)/valor;
				}
				break;
			case '*':
				return (float)calcula(node->LEFT)*calcula(node->RIGHT);
				break;
		}
	}
	if(node->CENTER->tipo==VARIAVEL)
	{	
		return (float)calcula(node->CENTER);
	}	
	return;
}

/**
* Create a new node with default values
**/

NO* newInteger(int valor)
{
    return newNode(TERMINAL, valor, NULL,NULL,NULL);
}

NO* newFloat(float valor)
{
    return newNode(TERMINAL, valor, NULL,NULL,NULL);
}


NO* newNode(int tipo, float valor, NO* LEFT, NO* CENTER, NO* RIGHT)
{
    // a new node
    NO* node = (NO*)(malloc(sizeof(NO)));

    // add the values
    node->tipo = tipo;
    node->valor = valor;
    node->LEFT = LEFT;
    node->CENTER = CENTER;
    node->RIGHT = RIGHT;

    return node;
}

int yylex(void);
void yyerror(char *);
%}


%union{
	int v_inteiro;
	float v_float;
	struct NO* v_no;
}

%token <v_inteiro> INTEGER
%token <v_float> FLOAT
%type <v_no> expr

%left '+' '-'
%left '*' '/'
//%left '^'

%%

program:
	program expr '\n' { print($2); printf("=%.2f\n",calcula($2)); }
	| 
	; 
expr:
	INTEGER 	{ $$ = newInteger($1); }
	| expr '+' expr { $$ = newNode(VARIAVEL,0,$1,newNode(OPERADOR,'+',NULL,NULL,NULL),$3);}
	| expr '-' expr { $$ = newNode(VARIAVEL,0,$1,newNode(OPERADOR,'-',NULL,NULL,NULL),$3);}
	| expr '*' expr { $$ = newNode(VARIAVEL,0,$1,newNode(OPERADOR,'*',NULL,NULL,NULL),$3);}
	| expr '/' expr { $$ = newNode(VARIAVEL,0,$1,newNode(OPERADOR,'/',NULL,NULL,NULL),$3);}
	| '(' expr ')' { $$ = newNode(VARIAVEL,0,newNode(OPERADOR,'(',NULL,NULL,NULL),$2,newNode(OPERADOR,')',NULL,NULL,NULL)); }
	| '-' INTEGER { $$ = newInteger(-$2); }

	| FLOAT 	{ $$ = newFloat($1); }	
	| '-' FLOAT { $$ = newFloat(-$2); }
	; 

%%
void yyerror(char *s) {
	fprintf(stderr, "%s\n", s);
}

int main(void) {
	yyparse();
	return 0;
}
