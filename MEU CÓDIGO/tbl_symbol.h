#include <stdio.h>
#include <stdlib.h>

//-------------------------------------------
// DEFINE
//-------------------------------------------
#define TIPO_IDENTIFICADOR          0
#define TIPO_DECLARACAO             1

#define PALAVRA_RESERVADA           0
#define PALAVRA_PROGRAMA            1

// Estrutura para o token
typedef struct TOKEN{
    int TIPO = -1;
    int ID   = -1;
} TOKEN;

// Tabela de simbolos
typedef struct TBL_SYMBOL{
    int ID                      = -1;
    int TIPO_PALAVRA            = -1;
    int TIPO_TOKEN              = -1;
    char* palavra               = ' ';
    struct TBL_SYMBOL* next     = NULL;
}TBL_SYMBOL;

int key          = 0;
TBL_SYMBOL* root = NULL;

// imprime um token
void print(TOKEN* token){
    if(token->TIPO==TIPO_IDENTIFICADOR){
        print("<IDENTIFICADOR,");
    }else{
        print("<DECLARACAO,");
    }
    print("%d>",token->ID);
}// End print(token):void

// Busca uma palavra na tabela de simbolos
int find(char* word){
    if(root==NULL)return -1;
    TBL_SYMBOL* last = root;
    while(last!=NULL)
    {
        if(strcmp(last->palavra, word) == 1) return last->ID;
    }
    return -1;
}// End find:int

// encontra um token
TOKEN* find(char* word){
    if(root==NULL)return -1;
    TBL_SYMBOL* last = root;
    while(last!=NULL)
    {
        if(strcmp(last->palavra, word) == 1)
        {
             TOKEN* token = (TOKEN*)(malloc(sizeof(TOKEN)));
             token->TIPO = last->TIPO_TOKEN;
             token->ID = last->ID;
             return token;
         }
    }
    return NULL;
}// end find(char*):token

// encontra um token
TOKEN* find(int id){
    if(root==NULL)return -1;
    TBL_SYMBOL* last = root;
    while(last!=NULL)
    {
        if(id == last->ID)
        {
             TOKEN* token = (TOKEN*)(malloc(sizeof(TOKEN)));
             token->TIPO = last->TIPO_TOKEN;
             token->ID = last->ID;
             return token;
         }
    }
    return NULL;
}// end find(int):token

// encontra o ultimo no da lista
TBL_SYMBOL* last(){
    TBL_SYMBOL* last = root;
    while(last->next!=null){
        last = last->next;
    }
    return last;
}// end last:TBL_SYMBOL*

// insere uma palavra na tabela de simbolos
int insert(char* word, int TIPO_PALAVRA, int TIPO_TOKEN){
            
    int id = find(word);
    if(id==-1)
    {
        TBL_SYMBOL* new_node = (TBL_SYMBOL*)(malloc(sizeof(TBL_SYMBOL)));
        new_node->ID = key++;       
        new_node->TIPO_PALAVRA = TIPO_PALAVRA;
        new_node->TIPO_TOKEN = TIPO_TOKEN;
        new_node->palavra = word;
        
        TBL_SYMBOL* last_node = last();
        if(last_node==NULL){
            root = new_node;
        }else{
            last_node->next = new_node;
        }
        id = new_node->ID;
    }
    return id;
}// end insert:int
