#include <stdio.h>
#include <stdlib.h>
#include "tbl_bymbol.h"

typedef struct NODE{
    int ID              = -1;
    TOKEN* token        = NULL;
    struct NODE* left,right;       
}NODE;

int key    = 0;
NODE* root = NULL;

// cria um novo no
NODE* new_node(TOKEN* token, NODE* left, NODE* right){
    NODE* node = (NODE*)(malloc(sizeof(NODE*)));
    node->token = token;
    node->left = left;
    node->right = right;
    node->ID = key++;
    if(root==NULL){
        root = NODE;
    }
    return NODE;
}// End new_node:node

// imprime um node
void print(NODE* node){
    if(node!=null){
        // classe tbl_symbol
        print(node->TOKEN);
        // classe local
        print(node->left);
        print(node->right);        
    }
}// print:void
