%{
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

int tamanho = 0;


// the VALUE struct
typedef union VALOR
{

    int v_int;
    double v_double;
    float v_float;
    char* v_char;

} VALOR;

// the SYMBOL struct
typedef struct SIMBOLO
{

    int id;
    char* nome;
    char* tipo;

    union VALOR* valor;

    struct SIMBOLO* PROXIMO;
    struct SIMBOLO* ANTERIOR;

} SIMBOLO;

SIMBOLO* TABELA;

// search in the symbol table
SIMBOLO* buscar(char* valor)
{
    // the last symbol
    SIMBOLO* ANT = TABELA;

// is the value to find
    if(strcmp(ANT->nome,valor)==0)
    {
        return ANT;
    }

    // while the next isn't NULL
    while(ANT->PROXIMO!=NULL)
    {
        // is the value to find
        if(strcmp(ANT->nome,valor)==0)
        {
            return ANT;
        }
        ANT = ANT->PROXIMO;
    }
    // RETURN NULL
    return NULL;
}


// init a new node
SIMBOLO* INIT()
{
    SIMBOLO* simbolo = (SIMBOLO*)malloc(sizeof(SIMBOLO));
    simbolo->id = -1;
    return simbolo;
}

// constructor to a new symbol
SIMBOLO* NEW(char* nome, char* tipo, VALOR* valor)
{
    SIMBOLO* no = INIT();
    no->nome = (char*)malloc(sizeof(nome)+1);
    strcpy(no->nome,nome);
    no->tipo = tipo;
    no->valor = valor;
}

VALOR* NEW_STRING(char* valor)
{
    VALOR* retorno = (VALOR*)malloc(sizeof(VALOR));
    retorno->v_char = (char*)malloc(sizeof(valor)+1);
    strcpy(retorno->v_char,valor);

    return retorno;
}

VALOR* NEW_DOUBLE(char* valor)
{
    VALOR* retorno = (VALOR*)malloc(sizeof(VALOR));
    retorno->v_double = atof(valor);    

    return retorno;
}

VALOR* NEW_FLOAT(char* valor)
{
    VALOR* retorno = (VALOR*)malloc(sizeof(VALOR));
    retorno->v_float = atof(valor);    

    return retorno;
}

VALOR* NEW_INT(char* valor)
{
    VALOR* retorno = (VALOR*)malloc(sizeof(VALOR));
    retorno->v_int = atoi(valor);    

    return retorno;
}

void printValor(SIMBOLO* valor)
{
    if(valor->tipo=="STRING"||valor->tipo=="LOGICA"||valor->tipo=="ATRIBUICAO"||valor->tipo=="OPERADOR"|valor->tipo=="DELIMITADOR")
    {
        printf("VALOR: %s\n",valor->valor->v_char);
    }
    else if(valor->tipo=="INT")
    {
        printf("VALOR: %d\n",valor->valor->v_int);
    }
    else if(valor->tipo=="DOUBLE")
    {
        printf("VALOR: %f\n",valor->valor->v_double);
    }
    else if(valor->tipo=="FLOAT")
    {
        printf("VALOR: %f\n",valor->valor->v_float);
    }
}

void imprimir()
{
    printf("\n==================\n");
    printf("TABELA DE SÍMBOLOS\n");
    printf("==================\n");
    SIMBOLO* ant = TABELA;
    while(ant!=NULL)
    {
        printf("ID: %d\n",ant->id);
        printf("NOME: %s\n",ant->nome);
        printf("TIPO: %s\n",ant->tipo);
        printValor(ant);
        ant = ant->PROXIMO;
        printf("\n");
    }
    printf("\n------------------\n");
}

// insert a new symbol
int inserir(SIMBOLO* valor)
{
    if(TABELA==NULL)
    {
        TABELA = valor;
        valor->id = ++tamanho;
        imprimir(TABELA);
        return valor->id;
    }
    else
    {
        // variable to store the last node
        SIMBOLO* ANT = TABELA;
        // gets if the symbol is in the table
        SIMBOLO* busca = buscar(valor->nome);

        if(busca==NULL)
        {

            // gets the last symbol
            while(ANT->PROXIMO!=NULL)
            {
                ANT = ANT->PROXIMO;
            }
            // add the value
            ANT->PROXIMO = valor;

            // create a new ID
            valor->id = ++tamanho;

            imprimir(TABELA);

            return valor->id;

        }// if
        imprimir(TABELA);
        return busca->id;
    }
}

%}

PALAVRA ([a-zA-Z][a-zA-Z0-9]*|"{"|"}"|"("|")"|"["|"]")
INTEIRO ("-"|"+")?[0-9]+
DOUBLE ("-"|"+")?([0-9]+)?\.[0-9][0-9]+
FLOAT ("-"|"+")?([0-9]+)?\.[0-9]+
OPERADOR ("+"|"-"|"*"|"/")
LOGICA ("<"|">"|"=="|"<="|">="|"|"|"||"|"&"|"&&")
ATRIBUICAO "="
DELIMITADOR ";"

%%
{INTEIRO}	 inserir(NEW(yytext,"INT",NEW_INT(yytext)));
{PALAVRA}	 inserir(NEW(yytext,"STRING",NEW_STRING(yytext)));
{DOUBLE}	 inserir(NEW(yytext,"DOUBLE",NEW_DOUBLE(yytext)));
{FLOAT}	 inserir(NEW(yytext,"FLOAT",NEW_FLOAT(yytext)));
{OPERADOR}	 inserir(NEW(yytext,"OPERADOR",NEW_STRING(yytext)));
{LOGICA}	 inserir(NEW(yytext,"LOGICA",NEW_STRING(yytext)));
{ATRIBUICAO}	 inserir(NEW(yytext,"ATRIBUICAO",NEW_STRING(yytext)));
{DELIMITADOR}	 inserir(NEW(yytext,"DELIMITADOR",NEW_STRING(yytext)));
%%
