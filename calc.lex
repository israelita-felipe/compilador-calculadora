%{
#include <stdlib.h>
void yyerror(char *);
#include "y.tab.h"
%}

%%

[0-9]+ {
		yylval.v_inteiro = atoi(yytext);
		return INTEGER;
	}

[0-9]+\.[0-9]+ {
		yylval.v_float = atof(yytext);
		return FLOAT;
	}

[-+*/()\^\n] 	return *yytext;

[ \t] 	; /* skip whitespace */
. 	yyerror("invalid character");
%%

int yywrap(void) {
	return 1;
}

