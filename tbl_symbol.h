#include <stdio.h>
#include <stdlib.h>

//-------------------------------------------
// DEFINE
//-------------------------------------------
#define TIPO_IDENTIFICADOR          0
#define TIPO_DECLARACAO             1

#define PALAVRA_RESERVADA           0
#define PALAVRA_PROGRAMA            1

// Estrutura para o token
typedef struct TOKEN{
    int TIPO = -1;
    int ID   = -1;
} TOKEN;

// Tabela de simbolos
typedef struct TBL_SYMBOL{
    int ID                      = -1;
    int TIPO_PALAVRA            = -1;
    int TIPO_TOKEN              = -1;
    char* palavra               = ' ';
    struct TBL_SYMBOL* next     = NULL;
}TBL_SYMBOL;

int key          = 0;
TBL_SYMBOL* root = NULL;

// Busca uma palavra na tabela de simbolos
int find(char* word){
    if(root==NULL)return -1;
    TBL_SYMBOL* last = root;
    while(last!=NULL)
    {
        if(strcmp(last->palavra, word) == 1) return last->ID;
    }
    return -1;
}// End find
